package test;

import com.mchange.v2.c3p0.C3P0Registry;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@SuppressWarnings("ConstantConditions")
public final class Sample {

    private StandardServiceRegistry registry;
    private Session session;

    @Before
    public void setUp() {
        registry = new StandardServiceRegistryBuilder()
                .enableAutoClose()
                .applySetting("hibernate.c3p0.max_size", "1")
                .applySetting("hibernate.c3p0.min_size", "1")
                .applySetting("hibernate.connection.driver_class", org.h2.Driver.class.getCanonicalName())
                .applySetting("hibernate.connection.url", "jdbc:h2:mem:")
                .applySetting("hibernate.connection.username", "sa")
                .applySetting("hibernate.connection.password", "")
                .applySetting("hibernate.dialect", org.hibernate.dialect.H2Dialect.class.getCanonicalName())
//                .applySetting("hibernate.connection.pool_size", 1)
                .applySetting("hibernate.show_sql", true)
                .applySetting("hibernate.hbm2ddl.auto", "create")
                .build();

        SessionFactory sessionFactory = new MetadataSources(registry)
                .addAnnotatedClass(Person.class)
                .addAnnotatedClass(Phone.class)
                .buildMetadata()
                .buildSessionFactory();
        session = sessionFactory.openSession();


    }

    @After
    public void tearDown() {
        session.close();
        StandardServiceRegistryBuilder.destroy(registry);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void c3p0() {
        C3P0Registry.getPooledDataSources().forEach(e -> System.out.println("DataSource: " + e));
    }

    @Test
    public void test1() {
        session.beginTransaction();

        Person person = new Person();
        person.setName("John");
        Phone phone = new Phone();
        phone.setNumber("0123-456-789");
        Phone phone2 = new Phone();
        phone2.setNumber("987-654-3210");
        person.setPhones(Arrays.asList(phone, phone2));

        session.persist(person);

        @SuppressWarnings("JpaQlInspection")
        Person ret = session.createQuery(
                "select p from Person p where p.name = 'John'",
                Person.class
        ).stream().findAny().orElse(null);

        assertThat(ret.getName(), is("John"));
        assertThat(ret.getPhones().get(0).getNumber(), is("0123-456-789"));
        assertThat(ret.getPhones().get(1).getNumber(), is("987-654-3210"));

        session.getTransaction().commit();
    }

    @Test
    public void test2() {
        session.beginTransaction();
        Person person = new Person();
        person.setName("John");
        session.persist(person);
        int id = person.getId();
        session.getTransaction().commit();

        session.beginTransaction();
        person = session.get(Person.class, id);
        assertThat(person.getName(), is("John"));
        person.setName("Jack");
        session.getTransaction().commit();

        session.beginTransaction();
        person = session.get(Person.class, id);
        assertThat(person.getName(), is("Jack"));
        session.getTransaction().commit();
    }

    @Test
    public void test3() {
        session.beginTransaction();
        Person person = new Person();
        person.setName("John");
        session.persist(person);
        session.getTransaction().commit();

        session.beginTransaction();
        person = session.find(Person.class, person.getId());
        person.setName("Jack");
        session.getTransaction().rollback();
        session.beginTransaction();
        person = session.find(Person.class, person.getId());
        assertThat(person.getName(), is("John"));
        session.getTransaction().rollback();
    }

}
